from flask import Flask, request, make_response

import numpy as np
import pandas as pd
from io import StringIO
import os
import pickle
import csv

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

import gensim
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess
from gensim.models.keyedvectors import KeyedVectors

from keras.layers import Embedding

from keras.layers import Dense, Input, GlobalMaxPooling1D
from keras.layers import Conv1D, MaxPooling1D, Embedding
from keras.models import Model
from keras.layers import Input, Dense, Embedding, Conv2D, MaxPooling2D, Dropout,concatenate
from keras.layers.core import Reshape, Flatten
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
from keras.models import Model
from keras import regularizers

from tensorflow import keras

app = Flask(__name__)
app.config["DEBUG"] = True

@app.route('/hello_world')
def hello_world():
    return "Hello world!"


@app.route('/genres/train', methods=['POST'])
def train():
    print("Training started")

    skip = False

    if not skip:

        if not request.data:
            return "Training Data Missing"
        # creating the necessary directories
        if not os.path.exists("./challenge/models"):
            os.mkdir("./challenge/models")
        if not os.path.exists("./challenge/data"):
            os.mkdir("./challenge/data")

        # Save train data as csv file
        f = open("./challenge/data/train_data.csv", "wb")
        f.write(request.data)
        f.close()

        # create DataFrame of the training set
        train_data = pd.read_csv("./challenge/data/train_data.csv", sep=",")
        print(train_data.head(3))

        print(train_data.shape)
        print(train_data.isnull().sum())

        # make categories of genres
        train_data.genres = train_data.genres.apply(lambda x:x.split(' ')[0])
        genres = train_data.genres.unique()
        num_classes = len(genres)
        print("List of genres: ", genres)
        print("Number of genres: ", num_classes)
        dic_genre2label = {}
        dic_label2genre = {}
        for i, genre in enumerate(genres):
            dic_genre2label[genre] = i
            dic_label2genre[i] = genre
        labels = train_data.genres.apply(lambda x:dic_genre2label[x])

        # save the mapping of label to genre
        dic_file = open("./challenge/data/dic_label2genre.pkl", "wb")
        pickle.dump(dic_label2genre, dic_file)
        dic_file.close()

        # divide train data into train data and validation
        val_data=train_data.sample(frac=0.2,random_state=189)
        train_data=train_data.drop(val_data.index)

        # Preprocess the synopsis
        NUM_WORDS=20000

        train_synopsis = train_data.synopsis

        tokenizer = Tokenizer(num_words=NUM_WORDS,filters='!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n\'',
                        lower=True)
        tokenizer.fit_on_texts(train_synopsis)
        sequences_train = tokenizer.texts_to_sequences(train_synopsis)
        sequences_valid = tokenizer.texts_to_sequences(val_data.synopsis)

        word_index = tokenizer.word_index
        print('Found %s unique tokens.' % len(word_index))

        # save the tokenizer for prediction use
        with open('./challenge/models/tokenizer.pickle', 'wb') as outfile:
            pickle.dump(tokenizer, outfile, protocol=pickle.HIGHEST_PROTOCOL)


        X_train = pad_sequences(sequences_train)
        X_val = pad_sequences(sequences_valid,maxlen=X_train.shape[1])
        y_train = to_categorical(np.asarray(labels[train_data.index]), num_classes=num_classes)
        y_val = to_categorical(np.asarray(labels[val_data.index]), num_classes=num_classes)

        print('Shape of X train and X validation tensor:', X_train.shape,X_val.shape)
        print('Shape of label train and validation tensor:', y_train.shape,y_val.shape)


        # Load google word2vec vectors
        word_vectors = KeyedVectors.load_word2vec_format('./challenge/data/GoogleNews-vectors-negative300.bin', binary=True)
        EMBEDDING_DIM=300

        vocabulary_size=min(len(word_index)+1,NUM_WORDS) # +1 because we padded with 0 value to have same sequence sizes
        embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
        for word, i in word_index.items():
            if i>=NUM_WORDS:
                continue
            try:
                # try to fetch the emmbedding vector
                embedding_vector = word_vectors[word]
                embedding_matrix[i] = embedding_vector
            except KeyError:
                # it it does not exist => create a random one
                embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)

        # free memory space
        del(word_vectors)

        # create the embedding layer
        embedding_layer = Embedding(vocabulary_size,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                trainable=True)

        # Create the network and train it as long as validation loss goes down
        sequence_length = X_train.shape[1]
        filter_sizes = [3,4,5]
        num_filters = 100
        drop = 0.5

        inputs = Input(shape=(sequence_length,))
        embedding = embedding_layer(inputs)
        reshape = Reshape((sequence_length,EMBEDDING_DIM,1))(embedding)

        conv_0 = Conv2D(num_filters, (filter_sizes[0], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
        conv_1 = Conv2D(num_filters, (filter_sizes[1], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
        conv_2 = Conv2D(num_filters, (filter_sizes[2], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)

        maxpool_0 = MaxPooling2D((sequence_length - filter_sizes[0] + 1, 1), strides=(1,1))(conv_0)
        maxpool_1 = MaxPooling2D((sequence_length - filter_sizes[1] + 1, 1), strides=(1,1))(conv_1)
        maxpool_2 = MaxPooling2D((sequence_length - filter_sizes[2] + 1, 1), strides=(1,1))(conv_2)

        merged_tensor = concatenate([maxpool_0, maxpool_1, maxpool_2], axis=1)
        flatten = Flatten()(merged_tensor)
        reshape = Reshape((len(filter_sizes)*num_filters,))(flatten)
        dropout = Dropout(drop)(flatten)
        output = Dense(units=num_classes, activation='softmax',kernel_regularizer=regularizers.l2(0.01))(dropout)

        # this creates a model
        model = Model(inputs, output)

        # Train the model

        adam = Adam(lr=1e-3)

        model.compile(loss='categorical_crossentropy',
                optimizer=adam,
                metrics=['acc'])
        callbacks = [EarlyStopping(monitor='val_loss')]
        model.fit(X_train, y_train, batch_size=1000, epochs=10, verbose=1, validation_data=(X_val, y_val),
            callbacks=callbacks)

        model.save('./challenge/models/cnn_word2vec')

    return "Training done"

@app.route('/genres/predict', methods=['POST'])
def predict():
    print("Predicting")

    # Reload model
    model = keras.models.load_model('./challenge/models/cnn_word2vec')

    # load the trained tokenizer
    with open('./challenge/models/tokenizer.pickle', 'rb') as inputfile:
        tokenizer = pickle.load(inputfile)

    # Reload mapping of genres to labels
    dic_file = open("./challenge/data/dic_label2genre.pkl", "rb")
    dic_label2genre = pickle.load(dic_file)

    # Save test data as csv file
    f = open("./challenge/data/test_data.csv", "wb")
    f.write(request.data)
    f.close()

    # create DataFrame of the testing set
    test_data = pd.read_csv("./challenge/data/test_data.csv", sep=",")
    print(test_data.head(3))

    # Preprocess the synopsis
    NUM_WORDS=20000

    test_synopsis = test_data.synopsis

    sequences_test = tokenizer.texts_to_sequences(test_synopsis)
    word_index = tokenizer.word_index


    X_test = pad_sequences(sequences_test, maxlen=187)

    y_pred = model.predict(X_test, batch_size=1000)

    si = generate_prediction_csv(y_pred, dic_label2genre, test_data.movie_id)
    output = make_response(si.getvalue())
    output.headers["Content-type"] = "text/csv"

    return output


def generate_prediction_csv(y_pred, dic_label2genre, movie_ids):
    nb_rows = y_pred.shape[0]
    labels = np.array([[dic_label2genre[i] for i in range(y_pred.shape[1])]])

    y_labels = np.repeat(labels, y_pred.shape[0], axis = 0)

    permutation = y_pred.argsort(axis = 1)
    y_labels_permuted = np.array( [y_labels[i][permutation[i]] for i in range(nb_rows)])

    y_labels_permuted = np.flip(y_labels_permuted, axis=1)
    y_labels_permuted = y_labels_permuted[:,0:5]

    si = StringIO()
    cw = csv.writer(si, delimiter=',')
    cw.writerow(["movie_id","predicted_genres"])

    for i in range(nb_rows):
        ranked_movies = ""
        for j in range(y_labels_permuted.shape[1]):
            if(j < y_labels_permuted.shape[1]-1):
                ranked_movies += y_labels_permuted[i][j] + " "
            else:
                ranked_movies += y_labels_permuted[i][j]

        cw.writerow([str(movie_ids[i]), ranked_movies])

    return si

app.run()